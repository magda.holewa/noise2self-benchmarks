import numpy as np
import tensorflow as tf


class Masker():
    """ Object for masking with Tensorflow backend """

    def __init__(self, width=3, mode='interpolate'):
        self.grid_size = width
        self.n_masks = width ** 2

        self.mode = mode

        # initialize kernel for interpolation
        kernel = tf.convert_to_tensor([[0.5, 1.0, 0.5], [1.0, 0.0, 1.0], [0.5, 1.0, 0.5]], dtype='float64')
        self.kernel = kernel / tf.reduce_sum(kernel)

    def mask(self, X, i):
        """ Mask batch of images """
        phasex = i % self.grid_size
        phasey = (i // self.grid_size) % self.grid_size
        mask = np.zeros(shape=X.shape[-3:-1])
        mask[phasex::self.grid_size, phasey::self.grid_size] = 1
        mask = mask[np.newaxis, :, :, np.newaxis]

        if self.mode == 'interpolate':
            masked = self.interpolate_mask(X, mask)
        elif self.mode == 'zero':
            mask_inv = tf.ones_like(mask) - mask
            masked = X * mask_inv
        elif self.mode == 'random':
            mask_inv = tf.ones_like(mask) - mask
            rng = tf.random.uniform(minval=0.0, maxval=1.0, shape=X.shape)
            masked = X * mask_inv + rng * mask
        else:
            raise NotImplementedError
            
        net_input = masked

        return net_input, mask

    def interpolate_mask(self, tensor, mask):
        mask_inv = tf.ones_like(mask) - mask
        
        channels = tensor.shape[-1]
        kernel = self.kernel

        if channels == 3:
            kernel = tf.stack([kernel, kernel, kernel])
        else:
            kernel = tf.expand_dims(kernel, -1)
        kernel = tf.expand_dims(kernel, -1)

        pointwise_filter = tf.eye(channels, batch_shape=[1, 1], dtype='float64')
        filtered_tensor = tf.nn.separable_conv2d(tensor, kernel, pointwise_filter=pointwise_filter, strides=[1,1,1,1], padding='SAME')

        return filtered_tensor * mask + tensor * mask_inv
