from typing import Iterable, Tuple
import numpy as np
import h5py
import tensorflow as tf
from tensorflow.keras.models import load_model
from skimage.metrics import mean_squared_error, peak_signal_noise_ratio, structural_similarity
from tqdm import tqdm
from fire import Fire
import json

from util import load_img

def test_generator(h5dataset, batch_size=32, noise_level=0.1, return_noise_map=False):
    n_examples = h5dataset.shape[0]
    batch_idx = 0
    expand_channel = len(h5dataset.shape) == 3

    rs = np.random.default_rng(seed=0)

    while True:
        y = h5dataset[batch_idx*batch_size:(batch_idx+1)*batch_size]
        if (batch_idx + 2) * batch_size > n_examples:
            batch_idx = 0
        else:
            batch_idx += 1
        y = y.astype(np.float) / 255
        if expand_channel:
            y = np.expand_dims(y, axis=-1)

        if isinstance(noise_level, Iterable):
            noise = rs.uniform(noise_level[0], noise_level[1])
        else:
            noise = noise_level

        x = y + rs.normal(scale=noise, size=y.shape)
        x = np.clip(x, 0.0, 1.0)

        if return_noise_map:
            x = [x, np.full(shape=(batch_size, 1), fill_value=noise)]

        yield x, y


def test_generator_from_files(paths, batch_size=32, patches_per_image=8, patch_size=64, yield_single_image=False, noise_level=0.1, grayscale=False, scale=1.0, return_noise_map=False):
    paths = np.array(paths, dtype='str')
    n_examples = len(paths)
    batch_idx = 0

    rs = np.random.default_rng(seed=0)

    if not yield_single_image:
        assert batch_size % patches_per_image == 0
        images_per_batch = batch_size // patches_per_image

    while True:
        if yield_single_image:
            y = load_img(paths[batch_idx], to4dim=True, grayscale=grayscale, scale=scale)
            if batch_idx == n_examples - 1:
                batch_idx = 0
            else:
                batch_idx += 1
        else:
            bp = paths[batch_idx*images_per_batch:(batch_idx+1)*images_per_batch]

            y = []

            for p in bp:
                img = load_img(p, to4dim=False, grayscale=grayscale)

                width, height = img.shape[0:2]
                xoffs = np.around(rs.uniform(0, width-patch_size, patches_per_image)).astype('int')
                yoffs = np.around(rs.uniform(0, height-patch_size, patches_per_image)).astype('int')

                for xo, yo in zip(xoffs, yoffs):
                    y.append(img[xo:xo+patch_size,yo:yo+patch_size,:])

            y = np.stack(y)

            if (batch_idx + 2) * images_per_batch > n_examples:
                batch_idx = 0
            else:
                batch_idx += 1

        if isinstance(noise_level, Iterable):
            noise = rs.uniform(noise_level[0], noise_level[1])
        else:
            noise = noise_level
        
        x = y + rs.normal(scale=noise, size=y.shape)
        x = np.clip(x, 0.0, 1.0)

        if return_noise_map:
            x = [x, np.full(shape=(1 if yield_single_image else 1, 1), fill_value=noise)]

        yield x, y


def __pad_array(arr, divider):
    if arr.shape[1] % divider == 0 and arr.shape[2] % divider == 0:
        return arr

    h = int(np.ceil(arr.shape[1] / divider) * divider - arr.shape[1])
    w = int(np.ceil(arr.shape[2] / divider) * divider - arr.shape[2])

    return np.pad(arr, ((0,0), (0, h), (0, w), (0,0)))


def pad_image_batch(batch, divider):
    if len(batch) == 2: # image and noise map
        return [__pad_array(batch[0], divider), batch[1]]
    else:
        return __pad_array(batch, divider)


def evaluate(dataset, model_path, noise_level, batch_size=32, patches_per_image=8, yield_single_image=False):
    file = None

    model = load_model(model_path)
    is_ffdnet = isinstance(model.inputs, list) and len(model.inputs) == 2

    if 'unet' in model.name.lower():
        model_pad = 16
    elif 'dae_skip' in model.name.lower():
        model_pad = 8
    elif 'dae' in model.name.lower():
        model_pad = 4
    elif is_ffdnet:
        model_pad = 2
    else:
        model_pad = 0

    if dataset.endswith('json'):
        with open(dataset, 'r') as fp:
            json_config = json.load(fp)
        
        if yield_single_image:
            items = len(json_config['test'])
            steps = items
            datagen = test_generator_from_files(json_config['test'], yield_single_image=True,
                noise_level=noise_level, grayscale=json_config['grayscale'], scale=json_config['scale'],
                return_noise_map=is_ffdnet)
        else:
            items = len(json_config['test']) * patches_per_image
            steps = items // batch_size
            datagen = test_generator_from_files(json_config['test'], yield_single_image=False,
                batch_size=batch_size, patches_per_image=patches_per_image,
                noise_level=noise_level, grayscale=json_config['grayscale'], scale=json_config['scale'],
                return_noise_map=is_ffdnet)
    else:
        if dataset == 'mnist':
            _, (images_test, _) = tf.keras.datasets.mnist.load_data(path="mnist.npz")
        else:
            file = h5py.File(dataset, 'r')
            images_test = file['test/clear']

        items = len(images_test)
        steps = items // batch_size

        datagen = test_generator(images_test, noise_level=noise_level, batch_size=batch_size, return_noise_map=is_ffdnet)

    errors = []
    psnr = []
    ssim = []

    errors_org = []
    psnr_org = []
    ssim_org = []

    for _ in tqdm(range(steps)):
        x_batch, y_batch = next(datagen)
        
        model_input = pad_image_batch(x_batch, model_pad) \
            if yield_single_image and model_pad else x_batch

        y_pred = model(model_input, training=False).numpy()

        if yield_single_image and model_pad:
            y_pred = y_pred[:, :x_batch[0].shape[1], :x_batch[0].shape[2], :] if is_ffdnet \
                else y_pred[:, :x_batch.shape[1], :x_batch.shape[2], :]

        errors += [mean_squared_error(y, yp) for y, yp in zip(y_batch, y_pred)]
        psnr += [peak_signal_noise_ratio(y, yp) for y, yp in zip(y_batch, y_pred)]
        ssim += [structural_similarity(y, yp, multichannel=True) for y, yp in zip(y_batch, y_pred)]

        if is_ffdnet:
            x_batch = x_batch[0]    # unpack (images, noise levels)

        errors_org += [mean_squared_error(y, x) for y, x in zip(y_batch, x_batch)]
        psnr_org += [peak_signal_noise_ratio(y, x) for y, x in zip(y_batch, x_batch)]
        ssim_org += [structural_similarity(y, x, multichannel=True) for y, x in zip(y_batch, x_batch)]

    if file:
        file.close()

    print('MSE original: %.5f' % np.mean(errors_org))
    print('MSE denoised: %.5f' % np.mean(errors))
    print('PSNR original: %.5f' % np.mean(psnr_org))
    print('PSNR denoised: %.5f' % np.mean(psnr))
    print('SSIM original: %.5f' % np.mean(ssim_org))
    print('SSIM denoised: %.5f' % np.mean(ssim))
    

if __name__ == '__main__':
    # evaluate(dataset='datasets/flowers_meta.json',
    #     model_path='trained_models/dncnn_20_1_flowers.h5',
    #     noise_level=0.2,
    #     yield_single_image=True
    # )
    Fire(evaluate)