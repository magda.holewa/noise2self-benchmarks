from numpy.core.defchararray import endswith
from util import draw_patches, load_img
import h5py
from PIL import Image
import os
import numpy as np
import glob
from itertools import product
from tqdm import tqdm
from fire import Fire
import json

np.random.seed(2137)


def __get_imgs_in_folder(dirname):
    paths = glob.glob(f'{dirname}/*.jpeg')
    paths += glob.glob(f'{dirname}/*.jpg')
    paths += glob.glob(f'{dirname}/*.png')

    return paths

def get_file_names(data_dir):
    splits = [f.name for f in os.scandir(data_dir) if f.is_dir()]

    valid_split = 0.1

    train_paths = []
    test_paths = []
    valid_paths = []
    
    if 'train' in splits:
        classes = [f.name for f in os.scandir(os.path.join(data_dir, splits[0])) if f.is_dir()]
        print(classes)
        if len(splits) == 2:
            # train and test
            for cl in classes:
                path = os.path.join(data_dir, 'train', cl)
                train_imgs = __get_imgs_in_folder(path)
                train_imgs = np.array(train_imgs, dtype=str)
                random_imgs = np.random.permutation(train_imgs)

                idx = int(valid_split * len(train_imgs))
                train_imgs, valid_imgs = list(random_imgs[idx:]), list(random_imgs[:idx])

                train_paths += train_imgs
                valid_paths += valid_imgs
        else:
            for cl in classes:
                path = os.path.join(data_dir, 'train', cl)
                train_paths += __get_imgs_in_folder(path)

                vpath = os.path.join(data_dir, 'valid', cl)
                valid_paths += __get_imgs_in_folder(vpath)
        
        for cl in classes:
            path = os.path.join(data_dir, 'test', cl)
            test_paths += __get_imgs_in_folder(path)
    elif len(splits) == 0:
        # directly images
        train_imgs = __get_imgs_in_folder(data_dir)
        train_imgs = np.array(train_imgs, dtype=str)
        random_imgs = np.random.permutation(train_imgs)

        idx = int(valid_split * len(train_imgs))
        train_paths, valid_paths, test_paths = list(random_imgs[2*idx:]), list(random_imgs[:idx]), list(random_imgs[idx:2*idx])
    else:
        print(splits)
        # folders are classes
        for cl in splits:
            path = os.path.join(data_dir, cl)
            train_imgs = __get_imgs_in_folder(path)
            train_imgs = np.array(train_imgs, dtype=str)
            random_imgs = np.random.permutation(train_imgs)

            idx = int(valid_split * len(train_imgs))
            train_imgs, valid_imgs, test_imgs = list(random_imgs[2*idx:]), list(random_imgs[:idx]), list(random_imgs[idx:2*idx])

            train_paths += train_imgs
            valid_paths += valid_imgs
            test_paths += test_imgs

    print(f'Train: {len(train_paths)}, valid: {len(valid_paths)}, test: {len(test_paths)}')
    return train_paths, valid_paths, test_paths


def __get_patches(img, patch_size):
    h, w = img.shape[:2]
    ny = h // patch_size
    nx = w // patch_size

    yoff = (h - patch_size * ny) // 2
    xoff = (w - patch_size * nx) // 2

    img = img[yoff:h-yoff,xoff:w-xoff]

    patches = [img[y*patch_size:(y+1)*patch_size, x*patch_size:(x+1)*patch_size] \
        for y, x in product(range(ny), range(nx))]

    return np.array(patches)


def file_to_patches(filename, patch_size=64, scale=1.0, grayscale = True):
    #arr = load_img(filename, grayscale, scale, to4dim=False)
    with Image.open(filename) as img:
        arr = img.convert('RGB')
        if scale < 1.0:
            arr = arr.resize((int(arr.width * scale), int(arr.height * scale)))
        if grayscale:
            arr = arr.convert('L')
        arr = np.asarray(arr)
    return __get_patches(arr, patch_size)


def process_subset(dsclear, paths, patch_size, grayscale, scale):
    train_cache_clear = []
    for i, train_file in enumerate(tqdm(paths)):
        train_clear = file_to_patches(train_file, patch_size, grayscale=grayscale, scale=scale)

        if len(train_clear):
            train_cache_clear.append(train_clear)

        if (i % 500 == 0 and i > 0) or i == len(paths) - 1:
            train_cache_clear = np.concatenate(train_cache_clear, axis=0)

            nslices = train_cache_clear.shape[0]

            if dsclear.shape[0] == 1:
                new_shape = nslices
            else:
                new_shape = nslices + dsclear.shape[0]

            dsclear.resize(new_shape, axis=0)
            
            dsclear[-nslices:] = train_cache_clear
            del train_cache_clear
            train_cache_clear = []


def main(data_dir, dataset_name, grayscale=False, scale=1.0, patch_size=64, paths_only=False):
    train_paths, valid_paths, test_paths = get_file_names(data_dir)

    json_name = (dataset_name[:-3] if dataset_name.endswith('.h5') else dataset_name) + '_meta.json'

    json_config = {
        'train': train_paths,
        'valid': valid_paths,
        'test': test_paths,
        'patch_size': patch_size,
        'grayscale': grayscale,
        'scale': scale
    }

    with open(json_name, 'w') as fp:
        json.dump(json_config, fp)
    
    if paths_only:
        return

    dsfile = h5py.File(dataset_name, 'w')

    dstrain = dsfile.create_group('train')
    dsclear = dstrain.create_dataset('clear', shape=(1,patch_size,patch_size) if grayscale else (1,patch_size,patch_size,3),
            maxshape=(None, patch_size, patch_size) if grayscale else (None,patch_size,patch_size,3), chunks=True, dtype='u1',
            compression='gzip', compression_opts=9)

    train_cache_clear = []
    for i, train_file in enumerate(tqdm(train_paths)):
        train_clear = file_to_patches(train_file, patch_size, grayscale=grayscale, scale=scale)

        if len(train_clear):
            train_cache_clear.append(train_clear)

        if (i % 500 == 0 and i > 0) or i == len(train_paths) - 1:
            train_cache_clear = np.concatenate(train_cache_clear, axis=0)

            nslices = train_cache_clear.shape[0]

            if dsclear.shape[0] == 1:
                new_shape = nslices
            else:
                new_shape = nslices + dsclear.shape[0]

            dsclear.resize(new_shape, axis=0)
            
            dsclear[-nslices:] = train_cache_clear
            del train_cache_clear
            train_cache_clear = []

    if len(valid_paths) < 1000:
        dsvalid = dsfile.create_group('valid')
        valid_clear = [file_to_patches(v, patch_size, grayscale=grayscale, scale=scale) for v in tqdm(valid_paths)]
        valid_clear = [v for v in valid_clear if len(v) > 0]
        valid_clear = np.concatenate(valid_clear, axis=0)

        dsclearvalid = dsvalid.create_dataset('clear', data=valid_clear, chunks=True, dtype='u1', compression='gzip', compression_opts=9)

        del valid_clear

        dstest = dsfile.create_group('test')
        test_clear = [file_to_patches(v, patch_size, grayscale=grayscale, scale=scale) for v in tqdm(test_paths)]
        test_clear = [v for v in test_clear if len(v) > 0]
        test_clear = np.concatenate(test_clear, axis=0)

        dscleartest = dstest.create_dataset('clear', data=test_clear, chunks=True, dtype='u1', compression='gzip', compression_opts=9)
    else:
        dsvalid = dsfile.create_group('valid')
        dsclearvalid = dsvalid.create_dataset('clear', shape=(1,patch_size,patch_size) if grayscale else (1,patch_size,patch_size,3),
            maxshape=(None, patch_size, patch_size) if grayscale else (None,patch_size,patch_size,3), chunks=True, dtype='u1',
            compression='gzip', compression_opts=9)
        process_subset(dsclearvalid, valid_paths, patch_size, grayscale, scale)

        dstest = dsfile.create_group('test')
        dscleartest = dstest.create_dataset('clear', shape=(1,patch_size,patch_size) if grayscale else (1,patch_size,patch_size,3),
            maxshape=(None, patch_size, patch_size) if grayscale else (None,patch_size,patch_size,3), chunks=True, dtype='u1',
            compression='gzip', compression_opts=9)
        process_subset(dscleartest, test_paths, patch_size, grayscale, scale)

    dsfile.close()

if __name__ == '__main__':
    #main('./flowers/flowers', 'flowers.h5', False)
    Fire(main)