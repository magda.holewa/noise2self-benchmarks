import numpy as np
from scipy.signal import convolve
from scipy.signal.signaltools import convolve2d


class ClassicMasker():
    """Object for masking with NumPy/SciPy backend"""

    def __init__(self, width=3, mode='interpolate'):
        self.grid_size = width
        self.n_masks = width ** 2
        self.mode = mode

        # initialize kernel for interpolation
        kernel = np.array([[0.5, 1.0, 0.5], [1.0, 0.0, 1.0], [0.5, 1.0, 0.5]])
        self.kernel = kernel / np.sum(kernel)

    def mask(self, X, i):
        """ Mask single image """
        phasex = i % self.grid_size
        phasey = (i // self.grid_size) % self.grid_size
        mask = np.zeros(shape=X.shape[0:2])
        mask[phasex::self.grid_size, phasey::self.grid_size] = 1

        if len(X.shape) == 3:
            mask = mask[:, :, np.newaxis]

        if self.mode == 'interpolate':
            masked = self.interpolate_mask(X, mask)
        elif self.mode == 'zero':
            mask_inv = np.ones_like(mask) - mask
            masked = X * mask_inv
        elif self.mode == 'random':
            mask_inv = np.ones_like(mask) - mask
            rng = np.random.uniform(0.0, 1.0, size=X.shape)
            masked = X * mask_inv + rng * mask
        else:
            raise NotImplementedError

        return masked, mask

    def interpolate_mask(self, img, mask):
        mask_inv = np.ones_like(mask) - mask
        
        kernel = self.kernel

        if len(img.shape) == 3:
            kernel = np.expand_dims(kernel, -1)

        filtered = convolve(img, kernel, mode='same')

        return filtered * mask + img * mask_inv
