from skimage.metrics import mean_squared_error, peak_signal_noise_ratio, structural_similarity
from skimage.restoration import denoise_nl_means, denoise_wavelet, estimate_sigma
from skimage.filters import median
from skimage.morphology import disk
from util import load_img, jinv_denoise_with_lambda, jinv_denoise_with_lambda_median, jinv_denoise, donut
import numpy as np
from classic_masker import ClassicMasker
from tqdm import tqdm
import json
from fire import Fire


def calibrate_dataset(meta_file, noise_level, mask_size, denoiser, parameter_name, min_value, max_value,
                      steps=10, evaluate=True, use_train=False, **kwargs):
    rs = np.random.default_rng(seed=0)

    with open(meta_file, 'r') as fp:
        json_config = json.load(fp)
        
    use_median = False
    denoiser_kw = kwargs
    
    if denoiser == 'nlm':
        denoiser = denoise_nl_means
        denoiser_kw['multichannel'] = True
    elif denoiser == 'wavelet':
        denoiser = denoise_wavelet
        denoiser_kw['multichannel'] = True
    elif denoiser == 'median':
        denoiser = median
        use_median = True

    param_values = np.linspace(min_value, max_value, steps)
    losses = np.zeros_like(param_values)

    dataset = 'train' if use_train else 'valid'

    for img_path in tqdm(json_config[dataset]):

        img = load_img(img_path, grayscale=json_config['grayscale'], to4dim=False, scale=json_config['scale'])

        if noise_level > 0:
            noisy = img + rs.normal(scale=noise_level, size=img.shape)
            noisy = np.clip(noisy, 0.0, 1.0)
        else:
            noisy = img

        for i, p in enumerate(param_values):
            if use_median:
                x = denoiser(noisy, np.expand_dims(donut(p), -1), **denoiser_kw)
            else:
                step_kw = denoiser_kw
                step_kw[parameter_name] = p
                x = jinv_denoise(noisy, denoiser, masker_size=mask_size, **step_kw)
            loss = mean_squared_error(x, noisy)

            losses[i] += loss

    losses /= len(json_config['valid'])
    print(losses)

    best_param = param_values[np.argmin(losses)]
    print(f'Best value for parameter {parameter_name}: {best_param}')

    if evaluate:
        if not use_median:
            denoiser_kw[parameter_name] = best_param

        mse_classic, psnr_classic, ssim_classic = [], [], []
        mse_jinv, psnr_jinv, ssim_jinv = [], [], []
        mse_plus, psnr_plus, ssim_plus = [], [], []

        for img_path in tqdm(json_config['test']):
            img = load_img(img_path, grayscale=json_config['grayscale'], to4dim=False, scale=json_config['scale'])
            if noise_level > 0:
                noisy = img + rs.normal(scale=noise_level, size=img.shape)
                noisy = np.clip(noisy, 0.0, 1.0)
            else:
                noisy = img

            if use_median:
                classic = denoiser(noisy, np.expand_dims(disk(best_param),-1), **denoiser_kw)
                jinv = denoiser(noisy, np.expand_dims(donut(best_param),-1), **denoiser_kw)
                jinv_plus = jinv_denoise_with_lambda_median(noisy, best_param, **denoiser_kw)
            else:
                classic = denoiser(noisy, **denoiser_kw)
                classic = np.reshape(classic, noisy.shape)
                jinv = jinv_denoise(noisy, denoiser, **denoiser_kw)
                jinv_plus = jinv_denoise_with_lambda(noisy, denoiser, stride=mask_size, **denoiser_kw)

            mse_classic.append(mean_squared_error(img, classic))
            psnr_classic.append(peak_signal_noise_ratio(img, classic))
            ssim_classic.append(structural_similarity(img, classic, multichannel=True))

            mse_jinv.append(mean_squared_error(img, jinv))
            psnr_jinv.append(peak_signal_noise_ratio(img, jinv))
            ssim_jinv.append(structural_similarity(img, jinv, multichannel=True))

            mse_plus.append(mean_squared_error(img, jinv_plus))
            psnr_plus.append(peak_signal_noise_ratio(img, jinv_plus))
            ssim_plus.append(structural_similarity(img, jinv_plus, multichannel=True))

        mse_classic = np.mean(mse_classic)
        psnr_classic = np.mean(psnr_classic)
        ssim_classic = np.mean(ssim_classic)

        mse_jinv = np.mean(mse_jinv)
        psnr_jinv = np.mean(psnr_jinv)
        ssim_jinv = np.mean(ssim_jinv)

        mse_plus = np.mean(mse_plus)
        psnr_plus = np.mean(psnr_plus)
        ssim_plus = np.mean(ssim_plus)

        print('Classic - MSE: %.4f\tPSNR:%.2f\tSSIM:%.2f' % (mse_classic, psnr_classic, ssim_classic))
        print('J-inv   - MSE: %.4f\tPSNR:%.2f\tSSIM:%.2f' % (mse_jinv, psnr_jinv, ssim_jinv))
        print('J-inv+  - MSE: %.4f\tPSNR:%.2f\tSSIM:%.2f' % (mse_plus, psnr_plus, ssim_plus))


if __name__ == '__main__':
    Fire(calibrate_dataset)