from train_single_image import train_on_image
from skimage.metrics import mean_squared_error, peak_signal_noise_ratio, structural_similarity
from skimage.restoration import denoise_nl_means, denoise_wavelet, estimate_sigma
from skimage.filters import median
from skimage.morphology import disk
from util import load_img, jinv_denoise, jinv_denoise_with_lambda_median, jinv_denoise_with_lambda, donut
import numpy as np
import matplotlib.pyplot as plt
from classic_masker import ClassicMasker
from tqdm import tqdm
import json
from fire import Fire


def compare_jinv(img_path, noise_level, denoiser, mask_size=4, scale=1.0, grayscale=False, **kwargs):
    rs = np.random.default_rng(seed=0)
    img = load_img(img_path, grayscale=False, to4dim=False, scale=scale)
    noisy = img + rs.normal(scale=noise_level, size=img.shape)
    noisy = np.clip(noisy, 0.0, 1.0)

    if denoiser == 'nlm':
        denoiser = denoise_nl_means
    elif denoiser == 'wavelet':
        denoiser = denoise_wavelet

    denoiser_kw = kwargs
    denoiser_kw['multichannel'] = True

    x = jinv_denoise(noisy, denoiser, masker_size=mask_size, **denoiser_kw)
    lam = jinv_denoise_with_lambda(noisy, denoiser, masker_size=mask_size, **denoiser_kw)

    gtloss_jinv = mean_squared_error(img, x)
    psnr_jinv = peak_signal_noise_ratio(img, x)
    print('J-inv\tGTLoss: %.4f\tPSNR:%.2f' % (gtloss_jinv, psnr_jinv))

    gtloss_jinvp = mean_squared_error(img, lam)
    psnr_jinvp = peak_signal_noise_ratio(img, lam)
    print('J-inv+\tGTLoss: %.4f\tPSNR:%.2f' % (gtloss_jinvp, psnr_jinvp))

    cl = denoiser(noisy, **denoiser_kw)

    gtloss = mean_squared_error(img, x)
    psnr = peak_signal_noise_ratio(img, x)
    print('Normal\tGTLoss: %.4f\tPSNR:%.2f' % (gtloss, psnr))

    _, ax = plt.subplots(1, 5)
    ax[0].imshow(img, cmap='gray', vmin=0, vmax=1)
    ax[0].axis('off')
    ax[1].imshow(x, cmap='gray', vmin=0, vmax=1)
    ax[1].axis('off')
    ax[2].imshow(noisy, cmap='gray', vmin=0, vmax=1)
    ax[2].axis('off')
    ax[3].imshow(lam, cmap='gray', vmin=0, vmax=1)
    ax[3].axis('off')
    ax[4].imshow(cl, cmap='gray', vmin=0, vmax=1)
    ax[4].axis('off')
    plt.show()


def calibrate_single_image(img_path, noise_level, mask_size, denoiser, parameter_name, min_value, max_value,
                           steps=11, verbose=False, scale=1.0, grayscale=False, evaluate=True, compare_to_dnn=True,
                           dnn_steps=1000, model_path=None, mode='patches', **kwargs):
    rs = np.random.default_rng(seed=0)

    img = load_img(img_path, grayscale=grayscale, to4dim=False, scale=scale)
    noisy = img + rs.normal(scale=noise_level, size=img.shape)
    noisy = np.clip(noisy, 0.0, 1.0)

    denoiser_kw = kwargs

    if denoiser == 'nlm':
        denoiser = denoise_nl_means
        algo_name = 'NLM'
        denoiser_kw['multichannel'] = True
    elif denoiser == 'wavelet':
        denoiser = denoise_wavelet
        algo_name = 'Wavelet'
        denoiser_kw['multichannel'] = True
    elif denoiser == 'median':
        denoiser = median
        algo_name = 'Median'

    best_loss = np.inf
    best_img = noisy
    best_val = min_value

    for p in np.linspace(min_value, max_value, steps):
        if algo_name == 'Median':
            x = denoiser(noisy, np.expand_dims(donut(p), -1), **denoiser_kw)
        else:
            step_kw = denoiser_kw
            step_kw[parameter_name] = p
            x = jinv_denoise(noisy, denoiser, masker_size=mask_size, **step_kw)
        loss = mean_squared_error(x, noisy)

        if verbose:
            gtloss = mean_squared_error(img, x)
            psnr = peak_signal_noise_ratio(img, x)
            print('%s: %.4f \tSSLoss: %.4f\tGTLoss: %.4f\tPSNR:%.2f' 
                  % (parameter_name, p, loss, gtloss, psnr))
        else:
            print('%s: %.4f \tSSLoss: %.4f' % (parameter_name, p, loss))

        if loss < best_loss:
            best_loss = loss
            best_img = x
            best_val = p

    print(f'Best value for parameter {parameter_name}: {best_val}')

    if evaluate:
        if algo_name == 'Median':
            classic = denoiser(noisy, np.expand_dims(disk(best_val),-1), **denoiser_kw)
            classic = np.reshape(classic, noisy.shape)
            jinv_plus = jinv_denoise_with_lambda_median(noisy, best_val, **denoiser_kw)
        else:
            denoiser_kw[parameter_name] = best_val
            classic = denoiser(noisy, **denoiser_kw)
            classic = np.reshape(classic, noisy.shape)
            jinv_plus = jinv_denoise_with_lambda(noisy, denoiser, stride=mask_size, **denoiser_kw)

        mse = mean_squared_error(img, classic)
        psnr = peak_signal_noise_ratio(img, classic)
        ssim = structural_similarity(img, classic, multichannel=True)
        print('Classic - MSE: %.4f\tPSNR:%.2f\tSSIM:%.2f' % (mse, psnr, ssim))

        mse = mean_squared_error(img, best_img)
        psnr = peak_signal_noise_ratio(img, best_img)
        ssim = structural_similarity(img, best_img, multichannel=True)
        print('J-inv -   MSE: %.4f\tPSNR:%.2f\tSSIM:%.2f' % (mse, psnr, ssim))

        mse = mean_squared_error(img, jinv_plus)
        psnr = peak_signal_noise_ratio(img, jinv_plus)
        ssim = structural_similarity(img, jinv_plus, multichannel=True)
        print('J-inv+ -  MSE: %.4f\tPSNR:%.2f\tSSIM:%.2f' % (mse, psnr, ssim))

        if compare_to_dnn:
            layers = 8
            dnn_denoised = train_on_image(np.expand_dims(img, 0),
                np.expand_dims(noisy, 0),
                mode=mode,
                layers=layers,
                steps=dnn_steps,
                mask_size=mask_size,
                model_path=model_path,
                lr=0.01)
            
            mse = mean_squared_error(img, dnn_denoised)
            psnr = peak_signal_noise_ratio(img, dnn_denoised)
            ssim = structural_similarity(img, dnn_denoised, multichannel=True)
            print('DnCNN -   MSE: %.4f\tPSNR:%.2f\tSSIM:%.2f' % (mse, psnr, ssim))

            _, ax = plt.subplots(1, 6)
            ax[5].imshow(dnn_denoised, cmap='gray', vmin=0, vmax=1)
            ax[5].axis('off')
            ax[5].set_title('DnCNN-' + str(layers))
        else:
            _, ax = plt.subplots(1, 5)

        ax[0].imshow(img, cmap='gray', vmin=0, vmax=1)
        ax[0].axis('off')
        ax[0].set_title('Original')

        ax[1].imshow(noisy, cmap='gray', vmin=0, vmax=1)
        ax[1].axis('off')
        ax[1].set_title('Noisy')
        ax[2].imshow(classic, cmap='gray', vmin=0, vmax=1)
        ax[2].axis('off')
        ax[2].set_title(algo_name)
        ax[3].imshow(best_img, cmap='gray', vmin=0, vmax=1)
        ax[3].axis('off')
        ax[3].set_title(algo_name + ' J-invariant')
        ax[4].imshow(jinv_plus, cmap='gray', vmin=0, vmax=1)
        ax[4].axis('off')
        ax[4].set_title(algo_name + ' J-invariant+')
        plt.show()

#compare_jinv(r'flowers\flowers\rose\326541992_d542103ca8_n.jpg')
# calibrate_single_image(
#     r'ZhangLabData\CellData\chest_xray\train\NORMAL\NORMAL-28501-0001.jpeg',
#     0.4,
#     4,
#     'nlm',
#     'h',
#     0.05,
#     0.5,
#     5,
#     verbose=True,
#     scale=0.25,
#     grayscale=True,
#     patch_size=5,
#     patch_distance=6
# )

if __name__ == '__main__':
    Fire(calibrate_single_image)