from tensorflow.python.types.core import Value
from util import load_img, show_example
from masker import Masker
import tensorflow as tf
from masker import Masker
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import h5py
import json
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.losses import mse
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.optimizers.schedules import ExponentialDecay
from tqdm import tqdm
from typing import Iterable
from fire import Fire

from models import build_autoencoder, build_dae_skip, build_dncnn, build_unet, build_ridnet, build_ffdnet


def n2t_generator(h5dataset, shuffle=True, batch_size=32, noise_level=0.1, return_noise_map=False):
    n_examples = h5dataset.shape[0]
    batch_idx = 0
    expand_channel = len(h5dataset.shape) == 3

    rs = np.random.default_rng(seed=0)

    while True:
        if not shuffle:
            y = h5dataset[batch_idx*batch_size:(batch_idx+1)*batch_size]
            if (batch_idx + 2) * batch_size > n_examples:
                batch_idx = 0
            else:
                batch_idx += 1
        else:
            idx = rs.permutation(n_examples)[:batch_size]
            idx = np.sort(idx)
            y = h5dataset[idx]
        y = y.astype(np.float) / 255
        if expand_channel:
            y = np.expand_dims(y, axis=-1)

        if isinstance(noise_level, Iterable):
            noise = rs.uniform(noise_level[0], noise_level[1])
        else:
            noise = noise_level

        x = y + rs.normal(scale=noise, size=y.shape)
        x = np.clip(x, 0.0, 1.0)

        if return_noise_map:
            x = [x, np.full(shape=(batch_size, 1), fill_value=noise)]

        yield x, y

def n2s_generator(h5dataset, shuffle=True, batch_size=32, mask_size=4, mask_mode='interpolate', noise_level=0.1, return_noise_map=False):
    masker = Masker(mask_size, mode=mask_mode)
    n_examples = h5dataset.shape[0]
    expand_channel = len(h5dataset.shape) == 3
    batch_idx = 0

    rs = np.random.default_rng(seed=0)

    while True:
        if not shuffle:
            y = h5dataset[batch_idx*batch_size:(batch_idx+1)*batch_size]
        else:
            idx = rs.permutation(n_examples)[:batch_size]
            idx = np.sort(idx)
            y = h5dataset[idx]
        y = y.astype(np.float) / 255
        if expand_channel:
            y = np.expand_dims(y, axis=-1)

        if isinstance(noise_level, Iterable):
            noise = rs.uniform(noise_level[0], noise_level[1])
        else:
            noise = noise_level

        y = y + rs.normal(scale=noise, size=y.shape)
        y = np.clip(y, 0.0, 1.0)

        x, mask = masker.mask(y, batch_idx)

        if not shuffle and (batch_idx + 2) * batch_size > n_examples:
            batch_idx = 0
        else:
            batch_idx += 1

        if return_noise_map:
            x = [x, np.full(shape=(batch_size, 1), fill_value=noise)]

        yield x, y, mask


def n2s_generator_from_files(paths, patch_size=64, patches_per_image=8, shuffle=True, batch_size=32, mask_size=4, mask_mode='interpolate', noise_level=0.1, grayscale=False, return_noise_map=False):
    masker = Masker(mask_size, mode=mask_mode)
    paths = np.array(paths, dtype='str')
    n_examples = len(paths)
    batch_idx = 0

    rs = np.random.default_rng(seed=0)
    assert batch_size % patches_per_image == 0
    images_per_batch = batch_size // patches_per_image

    while True:
        if not shuffle:
            bp = paths[batch_idx*images_per_batch:(batch_idx+1)*images_per_batch]
        else:
            idx = rs.permutation(n_examples)[:images_per_batch]
            bp = paths[idx]

        y = []

        if isinstance(noise_level, Iterable):
            noise = rs.uniform(noise_level[0], noise_level[1])
        else:
            noise = noise_level

        for p in bp:
            img = load_img(p, to4dim=False, grayscale=grayscale)
            img = img + rs.normal(scale=noise, size=img.shape)
            img = np.clip(img, 0.0, 1.0)

            width, height = img.shape[0:2]
            xoffs = np.around(rs.uniform(0, width-patch_size, patches_per_image)).astype('int')
            yoffs = np.around(rs.uniform(0, height-patch_size, patches_per_image)).astype('int')

            for xo, yo in zip(xoffs, yoffs):
                y.append(img[xo:xo+patch_size,yo:yo+patch_size,:])

        y = np.stack(y)
        x, mask = masker.mask(y, batch_idx)

        if not shuffle and (batch_idx + 2) * images_per_batch > n_examples:
            batch_idx = 0
        else:
            batch_idx += 1

        if return_noise_map:
            x = [x, np.full(shape=(batch_size, 1), fill_value=noise)]

        yield x, y, mask


def n2t_generator_from_files(paths, patch_size=64, patches_per_image=8, shuffle=True, batch_size=32, noise_level=0.1, grayscale=False, return_noise_map=False):
    paths = np.array(paths, dtype='str')
    n_examples = len(paths)
    batch_idx = 0

    rs = np.random.default_rng(seed=0)
    assert batch_size % patches_per_image == 0
    images_per_batch = batch_size // patches_per_image

    while True:
        if not shuffle:
            bp = paths[batch_idx*images_per_batch:(batch_idx+1)*images_per_batch]
        else:
            idx = rs.permutation(n_examples)[:images_per_batch]
            bp = paths[idx]

        y = []

        for p in bp:
            img = load_img(p, to4dim=False, grayscale=grayscale)

            width, height = img.shape[0:2]
            xoffs = np.around(rs.uniform(0, width-patch_size, patches_per_image)).astype('int')
            yoffs = np.around(rs.uniform(0, height-patch_size, patches_per_image)).astype('int')

            for xo, yo in zip(xoffs, yoffs):
                y.append(img[xo:xo+patch_size,yo:yo+patch_size,:])

        y = np.stack(y)

        if not shuffle and (batch_idx + 2) * images_per_batch > n_examples:
            batch_idx = 0
        else:
            batch_idx += 1

        if isinstance(noise_level, Iterable):
            noise = rs.uniform(noise_level[0], noise_level[1])
        else:
            noise = noise_level
        
        x = y + rs.normal(scale=noise, size=y.shape)
        x = np.clip(x, 0.0, 1.0)

        if return_noise_map:
            x = [x, np.full(shape=(batch_size, 1), fill_value=noise)]
        
        yield x, y


def train(dataset, mode, model_folder, model_arch, model_params, noise_level, batch_size=32, lr=0.01, decay=False, epochs=1, check_every_n_steps=0, patches_per_image=8):
    file = None

    if dataset.endswith('json'):
        with open(dataset, 'r') as fp:
            json_config = json.load(fp)
        
        items = len(json_config['train']) * patches_per_image
        vitems = len(json_config['valid']) * patches_per_image

        channels = 1 if json_config['grayscale'] else 3

        if mode == 'self':
            datagen = n2s_generator_from_files(json_config['train'], 
                patch_size=json_config['patch_size'],
                patches_per_image=patches_per_image,
                noise_level=noise_level,
                grayscale=json_config['grayscale'],
                shuffle=True,
                return_noise_map=(model_arch == 'ffdnet'))
            vd = n2s_generator_from_files(json_config['valid'],
                patch_size=json_config['patch_size'],
                patches_per_image=patches_per_image,
                noise_level=noise_level,
                grayscale=json_config['grayscale'],
                shuffle=False,
                return_noise_map=(model_arch == 'ffdnet'))

        else:
            datagen = n2t_generator_from_files(
                json_config['train'], patch_size=json_config['patch_size'], patches_per_image=patches_per_image,
                noise_level=noise_level, shuffle=True, return_noise_map=(model_arch == 'ffdnet'))
            vd = n2t_generator_from_files(
                json_config['valid'], patch_size=json_config['patch_size'], patches_per_image=patches_per_image,
                noise_level=noise_level, shuffle=False, return_noise_map=(model_arch == 'ffdnet'))

    else:
        if dataset == 'mnist':
            (images_train, _), (images_valid, _) = tf.keras.datasets.mnist.load_data(path="mnist.npz")
        
        else:
            file = h5py.File(dataset, 'r')
            images_train = file['train/clear']
            images_valid = file['valid/clear']
        
        channels = 1 if len(images_train.shape) == 3 else images_train.shape[-1]

        items = len(images_train)
        vitems = len(images_valid)

        if mode == 'self':
            datagen = n2s_generator(images_train, noise_level=noise_level, shuffle=True, return_noise_map=(model_arch == 'ffdnet'))
            vd = n2s_generator(images_valid, noise_level=noise_level, shuffle=False, return_noise_map=(model_arch == 'ffdnet'))

        else:
            datagen = n2t_generator(images_train, noise_level=noise_level, shuffle=True, return_noise_map=(model_arch == 'ffdnet'))
            vd = n2t_generator(images_valid, noise_level=noise_level, shuffle=False, return_noise_map=(model_arch == 'ffdnet'))

    if not model_params:
        model_params = dict()
    if model_arch == 'dncnn':
        model = build_dncnn(channels=channels, **model_params)
    elif model_arch == 'dae':
        model = build_autoencoder(channels=channels, **model_params)
    elif model_arch == 'dae_skip':
        model = build_dae_skip(channels=channels, **model_params)
    elif model_arch == 'unet':
        model = build_unet(channels=channels, **model_params)
    elif model_arch == 'ffdnet':
        model = build_ffdnet(channels=channels, **model_params)
    elif model_arch == 'ridnet':
        model = build_ridnet(channels=channels, **model_params)
    model.summary()

    steps_per_epoch = items // batch_size
    val_steps = vitems // batch_size
    print(steps_per_epoch)
    if check_every_n_steps == 0:
        check_every_n_steps = steps_per_epoch

    if isinstance(noise_level, Iterable):
        noise_str = f'{int(noise_level[0]*100)}-{int(noise_level[1]*100)}'
    else:
        noise_str = str(int(noise_level*100))

    model_path = model_folder + f'/{model.name}_{mode}_{noise_str}_{epochs}_{dataset.split(".")[0].split("/")[-1]}.h5'
    
    if decay:
        lr_schedule = ExponentialDecay(
            lr,
            decay_steps=10000,
            decay_rate=0.9,
            staircase=False)
    else:
        lr_schedule = lr

    optimizer = Adam(learning_rate=lr_schedule, epsilon=1e-8)

    best_loss = np.inf

    n2s = mode == 'self'

    for epoch in range(epochs):
        print("\nStart of epoch %d" % (epoch,))

        for step in range(steps_per_epoch):
            if n2s:
                x_data, y_data, mask = next(datagen)

                with tf.GradientTape() as tape:
                    logits = model(x_data, training=True)
                    loss_value = tf.reduce_mean(mse(y_data*mask, logits*mask))
            else:
                x_data, y_data = next(datagen)

                with tf.GradientTape() as tape:
                    logits = model(x_data, training=True)
                    loss_value = tf.reduce_mean(mse(y_data, logits))

            grads = tape.gradient(loss_value, model.trainable_weights)
            optimizer.apply_gradients(zip(grads, model.trainable_weights))

            if step % 20 == 0:
                print("Training loss [%d]: %.5f" % (step, float(loss_value)))

            if step == (steps_per_epoch - 1) or (step % check_every_n_steps == 0 and step > 0) :
                total_loss = 0
                for step in range(val_steps):
                    if n2s:
                        x_data, y_data, mask = next(vd)
                        logits = model(x_data, training=False)
                        loss_value = tf.reduce_mean(mse(y_data*mask, logits*mask))
                    else:
                        x_data, y_data = next(vd)
                        logits = model(x_data, training=False)
                        loss_value = tf.reduce_mean(mse(y_data, logits))

                    total_loss += loss_value
                
                total_loss = total_loss / val_steps
                print("Val loss: %.5f" % total_loss)
                if total_loss < best_loss:
                    model.save(model_path)
                    best_loss = total_loss
    ##

    if n2s:
        x_data, y_data, mask = next(vd)
        if model_arch == 'ffdnet':
            show_example(model, [y_data, x_data[1]], unwrap_x=True)
        else:
            show_example(model, y_data)
    else:
        x_data, y_data = next(vd)
        show_example(model, x_data, y_data, unwrap_x=(model_arch == 'ffdnet'))

    if file:
        file.close()


if __name__ == '__main__':
    # train(dataset='datasets/flowers_meta.json',
    #     mode='self',
    #     model_folder='trained_models',
    #     model_arch='dncnn',
    #     model_params={
    #         'd': 4,
    #         'residual': True
    #     },
    #     noise_level=0.2,
    #     check_every_n_steps=500,
    #     epochs=2
    # )
    Fire(train)