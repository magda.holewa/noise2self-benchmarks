from tensorflow.python.keras.saving.save import load_model
from masker import Masker
import tensorflow as tf
from masker import Masker
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from tensorflow.keras.losses import mse
from tensorflow.keras.optimizers import Adam
from skimage.metrics import peak_signal_noise_ratio

from models import build_dncnn
from util import show_example, draw_patches, load_img

def patch_generator(noisy, batch_size=8, patch_size=64, mask_size=4, mask_mode='interpolate'):
    """ Extracts random patches from single image """
    masker = Masker(mask_size, mode=mask_mode)
    
    batch_idx = 0
    w, h, c = noisy.shape[1:]

    while True:
        y = np.zeros(shape=(batch_size, patch_size, patch_size, c))
        for i in range(batch_size):
            rng_x = int(np.random.uniform(0, w - 64))
            rng_y = int(np.random.uniform(0, h - 64))
            y[i] = noisy[0, rng_x:rng_x+64, rng_y:rng_y+64, :]
        x, mask = masker.mask(y, batch_idx)
        batch_idx += 1

        yield x, y, mask


def single_img_generator(noisy, mask_size=4, mask_mode='interpolate'):
    """ Yields data from single image but different masks """
    masker = Masker(mask_size, mode=mask_mode)
    
    batch_idx = 0

    while True:
        x, mask = masker.mask(noisy, batch_idx)
        batch_idx += 1
        yield x, noisy, mask


def train_on_image(img, noisy, mode, layers, model_path=None, steps=1000, mask_size=4, lr=0.01):
    model = build_dncnn(d=layers, residual=False, channels=img.shape[-1])

    if mode == 'patches':
        datagen = patch_generator(noisy, mask_size=mask_size, mask_mode='interpolate')
    else:
        datagen = single_img_generator(noisy)
    
    ## TRAINING
    optimizer = Adam(learning_rate=lr, epsilon=1e-8)

    for step in range(steps):
        x_data, y_data, mask = next(datagen)

        with tf.GradientTape() as tape:
            logits = model(x_data, training=True)
            loss_value = tf.reduce_mean(mse(y_data*mask, logits*mask))

        grads = tape.gradient(loss_value, model.trainable_weights)
        optimizer.apply_gradients(zip(grads, model.trainable_weights))

        if step % 100 == 0 or step == steps - 1:
            print(
                "Training loss (for one batch) at step %d: %.5f"
                % (step, float(loss_value))
            )
            val_denoised = model(noisy, training=False)[0].numpy()
            print('PSNR: %.2f' % peak_signal_noise_ratio(img[0], val_denoised))
    ##

    if model_path:
        model.save(model_path)

    return val_denoised


def train(filename, mode, layers, noise_level, model_path):
    arr = load_img(filename)

    y = arr + np.random.normal(scale=noise_level, size=arr.shape)
    y = np.clip(y, 0.0, 1.0)

    model = build_dncnn(d=layers, residual=False, channels=arr.shape[-1])

    if mode == 'patches':
        datagen = patch_generator(y, mask_size=5, mask_mode='random')
    else:
        datagen = single_img_generator(y)

    steps_per_epoch = 1000
    epochs = 2

    ## TRAINING
    optimizer = Adam(learning_rate=0.01, epsilon=1e-8)

    vals = []

    for epoch in range(epochs):
        print("\nStart of epoch %d" % (epoch,))

        for step in range(steps_per_epoch):
            x_data, y_data, mask = next(datagen)

            with tf.GradientTape() as tape:
                logits = model(x_data, training=True)
                loss_value = tf.reduce_mean(mse(y_data*mask, logits*mask))

            grads = tape.gradient(loss_value, model.trainable_weights)
            optimizer.apply_gradients(zip(grads, model.trainable_weights))

            if step % 100 == 0:
                print(
                    "Training loss (for one batch) at step %d: %.5f"
                    % (step, float(loss_value))
                )
                val_denoised = model(y, training=False)[0,:,:,0].numpy()
                print('PSNR: %.2f' % peak_signal_noise_ratio(arr[0,:,:,0], val_denoised))
                vals.append(val_denoised)

    ##

    model.save(model_path)

    show_example(model, arr, y)
    draw_patches(vals, len(vals) // epochs, epochs)



# train(
#     filename = r'ZhangLabData\CellData\chest_xray\train\NORMAL\NORMAL-28501-0001.jpeg',
#     mode = 'full',
#     layers = 6,
#     noise_level = 0.2,
#     model_path = 'trained_models/single20.h5'
# )