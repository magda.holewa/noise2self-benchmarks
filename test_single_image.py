from PIL import Image
from tensorflow.keras.models import Model, load_model
from itertools import product
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.losses import mse
from skimage.metrics import peak_signal_noise_ratio, structural_similarity
from skimage.restoration import denoise_nl_means, estimate_sigma
from util import load_img

def masked_mse(y_true, y_pred):
    mask = y_true[1]
    loss = mse(y_true*mask, y_pred*mask)
    return tf.reduce_mean(loss)


#filename = r'ZhangLabData\CellData\chest_xray\train\NORMAL\NORMAL-32326-0001.jpeg'
filename = r'ZhangLabData\CellData\chest_xray\train\NORMAL\NORMAL-28501-0001.jpeg'
#filename = r'ZhangLabData\CellData\chest_xray\test\PNEUMONIA\BACTERIA-40699-0001.jpeg'
#filename = r'flowers\flowers\rose\326541992_d542103ca8_n.jpg'
filename = r'flowers\flowers\dandelion\16159487_3a6615a565_n.jpg'
#filename = r'FMDD\test\Confocal_BPAE_B\Confocal_BPAE_B_1.png'

grayscale = False

img = load_img(filename, grayscale, 0.5, to4dim=False)
img = img[0:512,0:512,:]

#img = img[100:500, 100:500, :]
noised = img + np.random.normal(scale=0.2, size=img.shape)
noised = np.clip(noised, 0.0, 1.0)

# estimate the noise standard deviation from the noisy image
sigma_est = np.mean(estimate_sigma(noised, multichannel=True))
print(f"estimated noise standard deviation = {sigma_est}")

model = load_model(r'dae_skip_self_0-20_3_flowers 1.h5')
is_ffdnet = isinstance(model.inputs, list) and len(model.inputs) == 2

model_input = np.expand_dims(noised, 0)
if is_ffdnet:
    model_input = [model_input, np.full(shape=(1, 1), fill_value=0.1)]


denoised = model.predict(model_input)
print(noised.shape)
print(denoised.shape)

f, ax = plt.subplots(1, 4)


patch_kw = dict(patch_size=5,      # 5x5 patches
                patch_distance=6,  # 13x13 search area
                multichannel=True)

# fast algorithm, sigma provided
denoise2_fast = denoise_nl_means(noised, h=0.16,
                                 fast_mode=True, **patch_kw)

ax[3].imshow(denoise2_fast, cmap='gray')
ax[3].set_title('NLM')
ax[3].axis('off')

ax[0].imshow(img, cmap='gray', vmin=0, vmax=1)
ax[0].set_title('clear')
ax[0].axis('off')

ax[1].imshow(noised, cmap='gray', vmin=0, vmax=1)
ax[1].set_title('noisy')
ax[1].axis('off')

ax[2].imshow(denoised[0], cmap='gray', vmin=0, vmax=1)
ax[2].set_title('model output')
ax[2].axis('off')
plt.show()

if grayscale:
    img = img[:,:,0]
    noised = noised[:,:,0]
    denoised = denoised[0,:,:,0]
else:
    denoised = denoised[0]

denoised = np.clip(denoised, 0.0, 1.0)

print(f'PSNR noise {peak_signal_noise_ratio(img, noised)}')
print(f'PSNR denoised {peak_signal_noise_ratio(img, denoised)}')
print(f'PSNR denoised {peak_signal_noise_ratio(img, denoise2_fast)}')
print(f'mse {tf.reduce_mean(mse(img, noised))}')
print(f'loss {tf.reduce_mean(mse(img, denoised))}')
print(f'loss NLM {tf.reduce_mean(mse(img, denoise2_fast))}')
print(f'SSIM noise {structural_similarity(img, noised, multichannel=True)}')
print(f'SSIM denoised {structural_similarity(img, denoised, multichannel=True)}')
print(f'SSIM denoised {structural_similarity(img, denoise2_fast, multichannel=True)}')

show_patch = False

if show_patch:
    f, ax = plt.subplots(1, 4)
    ax[0].imshow(img[200:500,200:500], cmap='gray', vmin=0, vmax=1)
    ax[0].set_title('clear')
    ax[0].axis('off')

    ax[1].imshow(noised[200:500,200:500], cmap='gray', vmin=0, vmax=1)
    ax[1].set_title('noisy')
    ax[1].axis('off')

    ax[2].imshow(denoised[200:500,200:500], cmap='gray', vmin=0, vmax=1)
    ax[2].set_title('model output')
    ax[2].axis('off')

    diff = denoised[200:500,200:500]
    diff = diff - noised[200:500,200:500]

    #ax[3].imshow(diff, cmap='gray')


    ax[3].imshow(denoise2_fast[200:500,200:500], cmap='gray')
    ax[3].set_title('NLM')
    ax[3].axis('off')

    plt.show()
