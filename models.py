import tensorflow as tf
from tensorflow.keras.layers import Conv2D, Input, BatchNormalization, Subtract, Add, Multiply, GlobalAveragePooling2D, UpSampling2D, MaxPooling2D, Concatenate, Reshape, Conv2DTranspose
from tensorflow.keras.activations import relu
from tensorflow.keras.models import Model


def build_dncnn(channels=1, d=20, residual=True, n_filters=64, kernel_size=3):
    input = Input(shape=(None,None,channels))
    x = Conv2D(n_filters, kernel_size, padding='same',
        activation='relu', use_bias=False)(input)
    for _ in range(d-2):
        x = Conv2D(n_filters, kernel_size, padding='same', use_bias=False)(x)
        x = BatchNormalization()(x)
        x = relu(x)
    x = Conv2D(channels, kernel_size, padding='same', use_bias=False)(x)

    if residual:
        x = Subtract()([input, x])

    model_name = 'dncnn_' + str(d)
    if residual:
        model_name = model_name + '_res'
    model = Model(inputs=input, outputs=x, name=model_name)
    return model


def build_autoencoder(channels=1, n_filters=64, kernel_size=3):
    """ https://arxiv.org/pdf/1608.04667.pdf """
    input = Input(shape=(None,None,channels))
    x = Conv2D(n_filters, kernel_size, padding='same', activation='relu', use_bias=False)(input)
    x = MaxPooling2D((2,2))(x)
    x = Conv2D(n_filters, kernel_size, padding='same', activation='relu', use_bias=False)(x)
    x = MaxPooling2D((2,2))(x)
    x = Conv2D(n_filters, kernel_size, padding='same', activation='relu', use_bias=False)(x)
    x = UpSampling2D((2,2))(x)
    x = Conv2D(n_filters, kernel_size, padding='same', activation='relu', use_bias=False)(x)
    x = UpSampling2D((2,2))(x)
    x = Conv2D(channels, kernel_size, padding='same', activation='relu', use_bias=False)(x)

    model = Model(inputs=input, outputs=x, name='dae')
    return model


def build_dae_skip(channels=1):
    """ https://www.sciencedirect.com/science/article/abs/pii/S0168900217314560 """
    input = Input(shape=(None, None, channels))
    enc1 = Conv2D(64, (3,3), strides=(2,2), padding='same', activation='relu')(input)
    enc2 = Conv2D(32, (3,3), strides=(2,2), padding='same', activation='relu')(enc1)
    x = Conv2D(16, (3,3), strides=(2,2), padding='same', activation='relu')(enc2)
    x = Conv2DTranspose(32, (3,3), strides=(2,2), padding='same', activation='relu')(x)
    x = Concatenate()([x, enc2])
    x = Conv2DTranspose(64, (3,3), strides=(2,2), padding='same', activation='relu')(x)
    x = Concatenate()([x, enc1])
    x = Conv2DTranspose(channels, (3,3), strides=(2,2), padding='same', activation='relu')(x)

    return Model(inputs=input, outputs=x, name='dae_skip')


def __unet_convblock(layer, out_channels, bias=True, batchnorm=True, kernel_size=3):
    layer = Conv2D(out_channels, kernel_size, padding='same', use_bias=bias)(layer)
    if batchnorm:
        layer = BatchNormalization()(layer)
    layer = relu(layer)

    layer = Conv2D(out_channels, kernel_size, padding='same', use_bias=bias)(layer)
    if batchnorm:
        layer = BatchNormalization()(layer)
    layer = relu(layer)

    return layer

def build_unet(channels=1):
    input = Input(shape=(None,None,channels))

    conv_down1 = __unet_convblock(input, 32)
    x = MaxPooling2D((2,2))(conv_down1)
    conv_down2 = __unet_convblock(x, 64)
    x = MaxPooling2D((2,2))(conv_down2)
    conv_down3 = __unet_convblock(x, 128)
    x = MaxPooling2D((2,2))(conv_down3)
    conv_down4 = __unet_convblock(x, 256)
    x = MaxPooling2D((2,2))(conv_down4)

    x = __unet_convblock(x, 256)

    x = Conv2DTranspose(256, 2, strides=(2,2), padding='same')(x)
    x = Concatenate(axis=-1)([x, conv_down4])
    x = __unet_convblock(x, 128)
    x = Conv2DTranspose(128, 2, strides=(2,2), padding='same')(x)
    x = Concatenate(axis=-1)([x, conv_down3])
    x = __unet_convblock(x, 64)
    x = Conv2DTranspose(64, 2, strides=(2,2), padding='same')(x)
    x = Concatenate(axis=-1)([x, conv_down2])
    x = __unet_convblock(x, 32)
    x = Conv2DTranspose(32, 2, strides=(2,2), padding='same')(x)
    x = Concatenate(axis=-1)([x, conv_down1])
    x = __unet_convblock(x, channels)

    model = Model(inputs=input, outputs=x, name='unet')
    return model


def build_ffdnet(channels=1, n_filters=64, kernel_size=(3,3), d=15):
    input = Input(shape=(None, None, channels))
    noise_input = Input(shape=(1,))

    # slicing block
    x1 = input[:,0::2,0::2,:]
    x2 = input[:,0::2,1::2,:]
    x3 = input[:,1::2,0::2,:]
    x4 = input[:,1::2,1::2,:]

    x5 = tf.ones_like(x1)[:,:,:,0]
    x5 = tf.expand_dims(x5, axis=-1)

    x5 = x5 * tf.reshape(noise_input, (-1, 1, 1, 1))

    x = Concatenate(axis=-1)([x1,x2,x3,x4,x5])

    ## CNN block
    x = Conv2D(n_filters, kernel_size, padding='same', activation='relu', use_bias=False)(x)
    for _ in range(d-2):
        x = Conv2D(n_filters, kernel_size, padding='same', use_bias=False)(x)
        x = BatchNormalization()(x)
        x = relu(x)
    x = Conv2D(channels*4, kernel_size, padding='same', use_bias=False)(x)

    # merging block
    o = UpSampling2D()(x)
    mask1 = tf.tile(tf.convert_to_tensor([[1,0],[0,0]], dtype='float32'), tf.shape(x)[1:3])
    mask1 = tf.expand_dims(mask1, 0)
    mask1 = tf.expand_dims(mask1, -1)
    mask2 = tf.tile(tf.convert_to_tensor([[0,1],[0,0]],  dtype='float32'), tf.shape(x)[1:3])
    mask2 = tf.expand_dims(mask2, 0)
    mask2 = tf.expand_dims(mask2, -1)
    mask3 = tf.tile(tf.convert_to_tensor([[0,0],[1,0]],  dtype='float32'), tf.shape(x)[1:3])
    mask3 = tf.expand_dims(mask3, 0)
    mask3 = tf.expand_dims(mask3, -1)
    mask4 = tf.tile(tf.convert_to_tensor([[0,0],[0,1]],  dtype='float32'), tf.shape(x)[1:3])
    mask4 = tf.expand_dims(mask4, 0)
    mask4 = tf.expand_dims(mask4, -1)

    o1 = Multiply()([o[:,:,:,0:channels], mask1])
    o2 = Multiply()([o[:,:,:,channels:channels*2], mask2])
    o3 = Multiply()([o[:,:,:,channels*2:channels*3], mask3])
    o4 = Multiply()([o[:,:,:,channels*3:channels*4], mask4])
    y = Add()([o1,o2,o3,o4])

    model_name = 'ffdnet_' + str(d)
    return Model(inputs=[input, noise_input], outputs=y, name=model_name)



def __eam(x, n_filters=64, reduction=16, ssc=False):

    r1 = Conv2D(n_filters, (3,3), padding='same', dilation_rate=1, activation='relu')(x)
    r1 = Conv2D(n_filters, (3,3), padding='same', dilation_rate=2, activation='relu')(r1)

    r2 = Conv2D(n_filters, (3,3), padding='same', dilation_rate=3, activation='relu')(x)
    r2 = Conv2D(n_filters, (3,3), padding='same', dilation_rate=4, activation='relu')(r2)

    layer = Concatenate(axis=-1)([r1, r2])
    layer = Conv2D(n_filters, (3,3), padding='same', activation='relu')(layer)

    layer = Add()([layer, x])

    r3 = Conv2D(n_filters, (3,3), padding='same', activation='relu')(layer)
    r3 = Conv2D(n_filters, (3,3), padding='same')(r3)

    layer = Add()([r3, layer])
    layer = relu(layer)

    r4 = Conv2D(n_filters, (3,3), padding='same', activation='relu')(layer)
    r4 = Conv2D(n_filters, (3,3), padding='same', activation='relu')(r4)
    r4 = Conv2D(n_filters, (1,1), padding='same')(r4)

    layer = Add()([r4, layer])
    layer = relu(layer)

    att = GlobalAveragePooling2D()(layer)
    att = Reshape(target_shape=(1,1,-1))(att)

    att = Conv2D(n_filters // reduction, (1,1), padding='same', activation='relu')(att)
    att = Conv2D(n_filters, (1,1), padding='same', activation='sigmoid')(att)

    layer = Multiply()([layer, att])

    if ssc:
        layer = Add()([x, layer])

    return layer


def build_ridnet(channels=1, n_filters=64, lsc=True, eamskip=False, ssc=False):
    input = Input(shape=(None,None,channels))

    x = Conv2D(n_filters, (3,3), padding='same', activation='relu')(input)

    layer = __eam(x, n_filters, ssc=ssc)
    layer = __eam(layer, n_filters, ssc=ssc)
    layer = __eam(layer, n_filters, ssc=ssc)
    layer = __eam(layer, n_filters, ssc=ssc)

    if eamskip:
        layer = Add()([x, layer])
    x = Conv2D(channels, (3,3), padding='same')(layer)

    if lsc:
        x = Add()([input, x])

    model_name = 'ridnet'
    if lsc:
        model_name += '_lsc'
    if eamskip:
        model_name += '_eamskip'
    if ssc:
        model_name += '_ssc'

    model = Model(inputs=input, outputs=x, name=model_name)
    return model
