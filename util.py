import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
from skimage.metrics import mean_squared_error
from skimage.restoration import estimate_sigma
from skimage.filters import median
from skimage.morphology import disk
from classic_masker import ClassicMasker

def show_example(model, noisy, clean=None, unwrap_x=False):
    denoised = model.predict(noisy)
    nplots = 3 if clean is not None else 2
    _, ax = plt.subplots(1, nplots)
    if unwrap_x:
        noisy = noisy[0]
    ax[0].imshow(noisy[0], cmap='gray', vmin=0, vmax=1)
    ax[0].axis('off')
    ax[1].imshow(denoised[0], cmap='gray', vmin=0, vmax=1)
    ax[1].axis('off')
    if nplots == 3:
        ax[2].imshow(clean[0], cmap='gray', vmin=0, vmax=1)
        ax[2].axis('off')
    plt.show()


def draw_patches(patches, nx, ny):
    # Utility to display fogure with splitted image

    _, axarr = plt.subplots(ny, nx) 

    if ny > 1 and nx > 1:
        for i, a in enumerate(axarr):
            for j, aa in enumerate(a):
                idx = nx * i + j
                aa.imshow(patches[idx], cmap='gray', vmin=0, vmax=1)
                aa.axis('off')
    else:
        for i, a in enumerate(axarr):
            a.imshow(patches[i], cmap='gray', vmin=0, vmax=1)
            a.axis('off')

    plt.show()



def load_img(filename, grayscale=True, scale=1.0, to4dim=True):
    with Image.open(filename) as img:
        arr = img.convert('RGB')
        if scale < 1.0:
            arr = arr.resize((int(arr.width * scale), int(arr.height * scale)))
        if grayscale:
            arr = arr.convert('L')

        arr = np.asarray(arr)
        if grayscale:
            arr = np.expand_dims(arr, -1)

    if to4dim:
        arr = np.expand_dims(arr, 0)

    arr = arr.astype(np.float) / 255
    return arr


def donut(r):
    r = int(r)
    selem = disk(r)
    selem[r][r] = 0
    return selem


def jinv_denoise(input, denoise_fcn, masker_size=4, **kwargs):
    """ Input: 2D or 3D array representing image (channels last) """
    masker = ClassicMasker(width=masker_size)
    clear_img = np.zeros(input.shape)

    for i in range(masker.n_masks):
        x, mask = masker.mask(input, i)
        d = denoise_fcn(x, **kwargs)
        d = np.reshape(d, x.shape)

        clear_img = clear_img + mask * d

    return clear_img


def jinv_denoise_with_lambda(input, denoise_fcn, stride=4, noise_var=None, **kwargs):
    if not noise_var:
        noise_var = np.square(np.mean(estimate_sigma(input, multichannel=True)))

    denoised = jinv_denoise(input, denoise_fcn, stride, **kwargs)
    lamb = noise_var / mean_squared_error(input, denoised)

    return lamb * denoised + (1-lamb) * input


def jinv_denoise_with_lambda_median(input, radius, noise_var=None, **kwargs):
    if not noise_var:
        noise_var = np.square(np.mean(estimate_sigma(input, multichannel=True)))

    denoised = median(input, np.expand_dims(donut(radius), -1), **kwargs)
    lamb = noise_var / mean_squared_error(input, denoised)

    return lamb * denoised + (1-lamb) * input